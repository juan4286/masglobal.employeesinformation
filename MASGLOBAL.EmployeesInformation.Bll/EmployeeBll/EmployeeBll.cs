﻿namespace MASGLOBAL.EmployeesInformation.Bll.EmployeeBll
{
	using MASGLOBAL.EmployeesInformation.Common.Entities.Employee;
	using MASGLOBAL.EmployeesInformation.Dal.Employee;
	using System;
	using System.Collections.Generic;
	using System.Threading.Tasks;

	public class EmployeeBll : IEmployeeBll
	{
		private IEmployeeRepository _objEmployeeRepository;

		public EmployeeBll(IEmployeeRepository objEmployeeRepository)
		{
			_objEmployeeRepository = objEmployeeRepository;
		}

		public async Task<IEnumerable<Employee>> GetAll()
		{
			try
			{
				var employees = await _objEmployeeRepository.GetAll();
				List<Employee> lstEmployee = new List<Employee>();
				//Here loop every employee to instance with factory and calculate anual salary
				foreach (var emp in employees)
				{
					Employee employee = EmployeeFactory.GetEmployee(emp);
					lstEmployee.Add(employee);
				}
				return lstEmployee;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public async Task<Employee> GetById(int id)
		{
			try
			{
				var emp = await _objEmployeeRepository.GetById(id);
				if (emp != null)
				{
					return EmployeeFactory.GetEmployee(emp);
				}
				return null;

			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}
	}
}
