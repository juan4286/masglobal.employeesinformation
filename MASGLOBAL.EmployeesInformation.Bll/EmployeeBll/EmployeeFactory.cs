﻿namespace MASGLOBAL.EmployeesInformation.Bll.EmployeeBll
{
	using MASGLOBAL.EmployeesInformation.Common.Entities.Employee;

	public class EmployeeFactory
	{
		public static Employee GetEmployee(EmployeeData objEmployee)
		{
			Employee employee = null;

			switch (objEmployee.contractTypeName)
			{
				case "HourlySalaryEmployee":
					employee = new HourlyEmployee()
					{
						hourlySalary = objEmployee.hourlySalary
					};

					break;
				case "MonthlySalaryEmployee":
					employee = new MonthlyEmployee()
					{
						monthlySalary = objEmployee.monthlySalary
					};
					break;
			}

			employee.Id = objEmployee.Id;
			employee.contractTypeName = objEmployee.contractTypeName;
			employee.name = objEmployee.name;
			employee.roleDescription = objEmployee.roleDescription;
			employee.roleId = objEmployee.roleId;

			return employee;
		}
	}
}
