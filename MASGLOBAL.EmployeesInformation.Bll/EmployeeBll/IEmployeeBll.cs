﻿namespace MASGLOBAL.EmployeesInformation.Bll.EmployeeBll
{
	using MASGLOBAL.EmployeesInformation.Common.Entities.Employee;
	using System.Collections.Generic;
	using System.Threading.Tasks;

	public interface IEmployeeBll
	{
		Task<IEnumerable<Employee>> GetAll();
		Task<Employee> GetById(int id);
	}
}
