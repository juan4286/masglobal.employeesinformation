﻿namespace MASGLOBAL.EmployeesInformation.Common.Entities.Employee
{
	public class Employee
	{
		public int Id { get; set; }
		public string name { get; set; }
		public string contractTypeName { get; set; }
		public int roleId { get; set; }
		public string roleName { get; set; }
		public string roleDescription { get; set; }
		public virtual decimal yearSalary { get; }
	}
}
