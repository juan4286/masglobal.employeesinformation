﻿namespace MASGLOBAL.EmployeesInformation.Common.Entities.Employee
{
	public class EmployeeData : Employee
	{
		public decimal hourlySalary { get; set; }
		public decimal monthlySalary { get; set; }
	}
}
