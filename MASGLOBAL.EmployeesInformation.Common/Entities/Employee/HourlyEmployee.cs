﻿namespace MASGLOBAL.EmployeesInformation.Common.Entities.Employee
{
	public class HourlyEmployee : Employee
	{
		public decimal hourlySalary { get; set; }
		public override decimal yearSalary => hourlySalary * 120 * 12;
	}
}
