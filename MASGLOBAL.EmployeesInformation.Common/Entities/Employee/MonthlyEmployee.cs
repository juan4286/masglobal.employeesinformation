﻿namespace MASGLOBAL.EmployeesInformation.Common.Entities.Employee
{
	public class MonthlyEmployee : Employee
	{
		public decimal monthlySalary { get; set; }
		public override decimal yearSalary => monthlySalary * 12;
	}
}
