﻿namespace MASGLOBAL.EmployeesInformation.Dal.Employee
{
	using MASGLOBAL.EmployeesInformation.Common.Entities.Employee;
	using System.Collections.Generic;
	using System.Configuration;
	using System.Linq;
	using System.Net.Http;
	using System.Threading.Tasks;
	using System.Web.Script.Serialization;

	public class EmployeeApiRepository : IEmployeeRepository
	{
		public async Task<IEnumerable<EmployeeData>> GetAll()
		{
			return await GetAllEmployeesFromService();
		}

		public async Task<EmployeeData> GetById(int id)
		{
			var employees = await GetAllEmployeesFromService();
			return employees.FirstOrDefault(x => x.Id == id);
		}

		async Task<IEnumerable<EmployeeData>> GetAllEmployeesFromService()
		{
			//Get path from Web.Config
			var path = ConfigurationManager.AppSettings["ServicePath"];
			var client = new HttpClient();
			var response = await client.GetAsync(path);
			var textResponse = await response.Content.ReadAsStringAsync();
			var serializer = new JavaScriptSerializer();
			var employees = serializer.Deserialize<IEnumerable<EmployeeData>>(textResponse);
			return employees;
		}
	}
}
