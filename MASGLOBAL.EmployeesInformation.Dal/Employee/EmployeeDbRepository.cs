﻿namespace MASGLOBAL.EmployeesInformation.Dal.Employee
{
	using MASGLOBAL.EmployeesInformation.Common.Entities.Employee;
	using System.Collections.Generic;
	using System.Configuration;
	using System.Data;
	using System.Data.SqlClient;
	using System.Linq;
	using System.Threading.Tasks;
	using Dapper;

	class EmployeeDbRepository : IEmployeeRepository
	{
		private IDbConnection _dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["dbConnectionConfig"].ConnectionString);

		public async Task<IEnumerable<EmployeeData>> GetAll()
		{
			var employees = await _dbConnection.QueryAsync<EmployeeData>("spGetEmployees", new { }, commandType: CommandType.StoredProcedure);

			return employees;
		}

		public async Task<EmployeeData> GetById(int id)
		{
			var employees = await _dbConnection.QueryAsync<EmployeeData>("spGetEmployees", new { idEmployee = id }, commandType: CommandType.StoredProcedure);

			return employees.FirstOrDefault();
		}
	}
}
