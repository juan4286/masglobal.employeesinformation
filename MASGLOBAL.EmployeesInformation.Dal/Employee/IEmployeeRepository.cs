﻿namespace MASGLOBAL.EmployeesInformation.Dal.Employee
{
	using MASGLOBAL.EmployeesInformation.Common.Entities.Employee;
	using System.Collections.Generic;
	using System.Threading.Tasks;

	public interface IEmployeeRepository
	{
		Task<IEnumerable<EmployeeData>> GetAll();
		Task<EmployeeData> GetById(int id);
	}
}
