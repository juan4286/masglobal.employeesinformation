﻿namespace MASGLOBAL.EmployeesInformation.Test.Employee
{
	using Microsoft.VisualStudio.TestTools.UnitTesting;
	using System.Collections.Generic;
	using System.Linq;
	using MASGLOBAL.EmployeesInformation.Common.Entities.Employee;
	using MASGLOBAL.EmployeesInformation.Bll.EmployeeBll;

	[TestClass]
	public class EmployeeTest
	{
		[TestMethod]
		public void TestYearSalary()
		{
			IEnumerable<EmployeeData> lstTestEmployees = new List<EmployeeData>()
			{
				new EmployeeData()
				{

					Id = 5,
					name = "David",
					roleId = 1,
					contractTypeName = "HourlySalaryEmployee",
					hourlySalary = 25000

				},
				new EmployeeData()
				{

					Id = 7,
					name = "Valeria",
					roleId = 1,
					contractTypeName = "MonthlySalaryEmployee",
					monthlySalary = 800000

				}
			};

			//Arrange
			decimal expectedAnualSalary1 = 36000000;
			decimal expectedAnualSalary2 = 9600000;

			//Act
			var lstEmployees = new List<Employee>();
			foreach (var emp in lstTestEmployees)
			{
				Employee employee = EmployeeFactory.GetEmployee(emp);
				lstEmployees.Add(employee);
			}

			//Assert
			Assert.AreEqual(lstEmployees.ElementAt(0).yearSalary, expectedAnualSalary1);
			Assert.AreEqual(lstEmployees.ElementAt(1).yearSalary, expectedAnualSalary2);
		}
	}
}
