﻿namespace MASGLOBAL.EmployeesInformation.Web.Controllers
{
	using MASGLOBAL.EmployeesInformation.Bll.EmployeeBll;
	using MASGLOBAL.EmployeesInformation.Common.Entities.Employee;
	using System.Collections.Generic;
	using System.Threading.Tasks;
	using System.Web.Http;

	public class EmployeeApiController : ApiController
    {
		private readonly IEmployeeBll _employeeBll;

		public EmployeeApiController(IEmployeeBll employeeBll)
		{
			_employeeBll = employeeBll;
		}

		// GET: api/EmployeeApi
		public async Task<IEnumerable<Employee>> Get()
		{
			return await _employeeBll.GetAll();
		}

		// GET: api/EmployeeApi/5
		public async Task<IEnumerable<Employee>> Get(int id)
		{
			List<Employee> lstEmployees = new List<Employee>();
			var employee = await _employeeBll.GetById(id);
			if (employee != null)
			{
				lstEmployees.Add(employee);
			}
			return lstEmployees;
		}
	}
}
